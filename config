#!/bin/bash

EMAILLE="zharif@cabangpokok.com"
WEBROOTPATH="/var/www/html"
NGINXPATH="/etc/nginx"
SITEENABLED="$NGINXPATH/sites-enabled"
SITEAVAILABLE="$NGINXPATH/sites-available"
TEMPLATE="$NGINXPATH/snippets"
SSLDHPARAM="$NGINXPATH/ssl/dhparam.pem"
DHPARAMCONF="$TEMPLATE/ssl-params.conf"
LEPATH="/etc/letsencrypt"
LIVECERT="$LEPATH/live"
BACKUPATH="$NGINXPATH/backup"

genVAR() # $1 input is fqdn, $2 input destination ip, $3 destination port
{
	FQDN=$1
	DSTIP=$2
	DSTPRT=$3
	SSLCONF="$TEMPLATE/ssl-$FQDN.conf"
	SSLFULLCHAIN="$LIVECERT/$FQDN/fullchain.pem"
	SSLPRIV="$LIVECERT/$FQDN/privkey.pem"
}

createHTTPSconf(){
	if [[ -f $DHPARAMCONF ]]; then
		genSSLPconf > $SSLCONF
		genHTTPSblock > $SITEAVAILABLE/$1
	else
		genDHPconf > $DHPARAMCONF
		genSSLPconf > $SSLCONF
		genHTTPSblock > $SITEAVAILABLE/$1
	fi
}

createHTTPconf(){
	genHTTPblock > $SITEAVAILABLE/$1
}

genDHPconf(){
	cat <<- EOF
	ssl_session_cache shared:SSL:10m;
	ssl_session_timeout 10m;
	ssl_protocols TLSv1.2;
	ssl_prefer_server_ciphers on;
	ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;
	ssl_stapling on;
	ssl_stapling_verify on;
	ssl_dhparam $SSLDHPARAM;
	ssl_ecdh_curve secp384r1;
	add_header Strict-Transport-Security "max-age=31536000; includeSubdomains";
	add_header X-Frame-Options DENY;
	add_header X-Content-Type-Options nosniff;
	EOF
}

genSSLPconf(){
	cat <<- EOF
	ssl_certificate $SSLFULLCHAIN;
	ssl_certificate_key $SSLPRIV;
    gzip off;
	EOF
}

genHTTPblock(){
	cat <<- EOF
	server {
	    listen 80;
	    listen [::]:80;
	    server_name $FQDN;
	    root $WEBROOTPATH;
	    location / {
	        proxy_set_header   X-Real-IP \$remote_addr;
	        proxy_set_header   Host      \$http_host;
	        proxy_pass         http://$DSTIP:$DSTPRT;
	    }
	    access_log /var/log/nginx/$FQDN-access.log;
	    error_log /var/log/nginx/$FQDN-error.log;
	}
	EOF
}

genHTTPSblock(){
	cat <<- EOF
	server {
	    listen 80;
	    listen [::]:80;	    
	    server_name $FQDN;
	    return 301 https://\$server_name\$request_uri;
	}
	server {
	    listen 443 ssl http2;
	    listen [::]:443 ssl http2;
	    include $SSLCONF;
	    include $DHPARAMCONF;
	    server_name $FQDN;
	    root $WEBROOTPATH;
	    location ~ /.well-known {
	                allow all;
	    }
	    location / {
	        proxy_set_header   X-Real-IP \$remote_addr;
	        proxy_set_header   Host      \$http_host;
	        proxy_pass         http://$DSTIP:$DSTPRT;
	    }
	    access_log /var/log/nginx/$FQDN-access.log;
	    error_log /var/log/nginx/$FQDN-error.log;
	}
	EOF
}

getIP()
{
	cat $SITEAVAILABLE/$1 | grep proxy_pass | grep -v "return 301" | awk '{print $NF}' | sed 's/;//g' | uniq
}

get-fqdn(){
	cat $SITEAVAILABLE/$1 | grep server_name | grep -v "return 301" | awk '{print $NF}' | sed 's/;//g' | uniq
}

contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

say () {
	if [ "$1" == "info" ];then
		echo -e "\e[34;1m[info]\e[0m $2"
	fi
	if [ "$1" == "error" ];then
		echo -e "\e[91;1m[error]\e[0m $2"
	fi
	if [ "$1" == "output" ];then
		echo -e "\e[32m[output]\e[0m $2"
	fi
}

confirm () {
	read -r -p "Are you sure? [y/N] " response
	if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]] 
		then	say info $1
	else
		exit 1
	fi
}